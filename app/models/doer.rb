class Doer < ActiveRecord::Base
  belongs_to :user

  belongs_to :city

  has_and_belongs_to_many :favorite_ins,
                          class_name: 'User',
                          join_table: :favorite_relationships,
                          foreign_key: :doer_id,
                          association_foreign_key: :user_id

  has_and_belongs_to_many :services

  has_many :orders

  has_many :calculation_data

  has_many :messages

  has_many :feedbacks

  has_many :payments

  has_many :open_orders

  has_many :visible_orders, through: :open_orders, class_name: 'Order', source: :order

  has_many :contacts, as: :subject
end

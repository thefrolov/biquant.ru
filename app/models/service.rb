class Service < ActiveRecord::Base
  has_and_belongs_to_many :categories

  has_and_belongs_to_many :doers

  has_many :orders

  has_many :calculation_pages
end

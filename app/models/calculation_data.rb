class CalculationData < ActiveRecord::Base
  belongs_to :doer
  belongs_to :calculation_block
end

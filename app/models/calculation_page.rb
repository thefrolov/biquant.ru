class CalculationPage < ActiveRecord::Base
  actable

  belongs_to :service

  has_many :calculation_blocks
end

class CalculationBlock < ActiveRecord::Base
  actable

  belongs_to :calculation_page

  has_many :calculation_data

  has_many :order_data
end

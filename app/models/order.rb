class Order < ActiveRecord::Base
  belongs_to :service

  belongs_to :customer

  belongs_to :doer

  has_many :order_data

  has_many :messages

  has_one :feedback

  has_many :order_images

  has_many :open_orders

  has_many :interested_doers, through: :open_orders, class_name: 'Doer', source: :doer
end

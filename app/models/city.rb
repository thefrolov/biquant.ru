class City < ActiveRecord::Base
  belongs_to :country

  has_many :doers

  has_many :customers
end

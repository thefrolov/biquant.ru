class User < ActiveRecord::Base
  has_one :customer
  has_one :doer

  has_and_belongs_to_many :favorite_doers,
                          class_name: 'Doer',
                          join_table: :favorite_relationships,
                          foreign_key: :user_id,
                          association_foreign_key: :doer_id

  has_many :system_messages
end

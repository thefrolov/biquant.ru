class Feedback < ActiveRecord::Base
  belongs_to :order

  belongs_to :doer

  belongs_to :customer
end

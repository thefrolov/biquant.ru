require 'test_helper'

class OpenOrderTest < ActiveSupport::TestCase
  test "open_orders" do
    d1 = Doer.create
    c1 = Customer.create
    o1 = Order.create customer: c1, date_start: Time.now
    p1 = OpenOrder.create order:o1, doer: d1

    assert d1.visible_orders.first.id == o1.id
    assert o1.interested_doers.first.id == d1.id
  end
end

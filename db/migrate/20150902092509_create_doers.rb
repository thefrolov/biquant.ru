class CreateDoers < ActiveRecord::Migration
  def change
    create_table :doers do |t|
      t.actable

      t.belongs_to :user
      t.belongs_to :city

      t.integer :feedbacks_counter
      t.integer :feedbacks_positive_counter
      t.integer :feedbacks_negative_counter
      t.decimal :rating
      t.boolean :avalible

      t.timestamps null: false
    end
  end
end

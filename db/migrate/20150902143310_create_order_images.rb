class CreateOrderImages < ActiveRecord::Migration
  def change
    create_table :order_images do |t|
      t.belongs_to :order
      t.timestamps null: false
    end
  end
end

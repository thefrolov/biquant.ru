class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.belongs_to :service
      t.belongs_to :customer
      t.belongs_to :doer

      t.string :cid
      t.boolean :doer_accepted, default: false
      t.boolean :customer_accepted, default: false

      t.datetime :date_start, null: false
      t.datetime :date_end, null: true

      t.timestamps null: false
    end
  end
end

class CreateCBlockBlockCheckboxBlocks < ActiveRecord::Migration
  def change
    create_table :c_block_block_checkbox_blocks do |t|
      t.references :page
      t.timestamps null: false
    end
  end
end

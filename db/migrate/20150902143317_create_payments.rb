class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.belongs_to :doer
      t.timestamps null: false
    end
  end
end

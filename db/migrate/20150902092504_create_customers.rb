class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.belongs_to :user
      t.belongs_to :city
      t.timestamps null: false
    end
  end
end

class CreateCalculationData < ActiveRecord::Migration
  def change
    create_table :calculation_data do |t|
      t.belongs_to :calculation_block
      t.timestamps null: false
    end
  end
end

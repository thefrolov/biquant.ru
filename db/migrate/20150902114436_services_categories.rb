class ServicesCategories < ActiveRecord::Migration
  def change
    # create_table :categories_services, id:false do |t|
    #   t.belongs_to :category
    #   t.belongs_to :service
    # end
    
    create_join_table :categories, :services do |t|
      t.index :category_id
      t.index :service_id
    end
  end
end

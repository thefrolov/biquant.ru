class CreateSystemMessages < ActiveRecord::Migration
  def change
    create_table :system_messages do |t|
      t.belongs_to :user
      t.timestamps null: false
    end
  end
end

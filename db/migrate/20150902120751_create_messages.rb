class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.belongs_to :customer
      t.belongs_to :doer
      t.belongs_to :order

      t.boolean :was_read, null: false, default: false

      t.timestamps null: false
    end
  end
end
